# Data Management Panel Discussion

Slides from the panel discussion on Research Data Management at EMBL, October 30th 2018.

Files:
- [__CombinedResearchDataManagementPanelSlides.pdf__](/CombinedResearchDataManagementPanelSlides.pdf): All of the files below combined into a single PDF file.
- [__DataManagementPlanFunders.pdf__](/DataManagementPlanFunders.pdf): An introduction to funders' requirements for data management in research projects, by Chantal Brueggemann.
- [__GenomeBiologyDataManagement.pdf__](/GenomeBiologyDataManagement.pdf): An introduction to research data management solutions provided by [Genome Biology Computational Support](https://gbcs.embl.de/portal/tiki-index.php), by Charles Girardot.
- [__ITServicesDataManagementPanel.pdf__](/ITServicesDataManagementPanel.pdf): An introduction to support for data management from EMBL IT Services, by Matthias Helmling.
- [__ImageDataManagement.pdf__](/ImageDataManagement.pdf): An introduction to systems available for management of imaging data at EMBL, by Jean-Karim Hériché.
- [__IntroResearchDataManagementEMBL.pdf__](/IntroResearchDataManagementEMBL.pdf): A general introduction to data management at EMBL, the FAIR data principles, and considerations for open access to data, by Ioanna Ydraiou.